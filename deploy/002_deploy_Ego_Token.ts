import {HardhatRuntimeEnvironment} from 'hardhat/types';
import {DeployFunction} from 'hardhat-deploy/types';

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const {deployments, getNamedAccounts} = hre;
  const {deploy} = deployments;

  const {admin} = await getNamedAccounts();
  let name = "Testnet EGO (tEGO)"
  let symbol = "tEGO"
  
  let contract = await deploy('EgoToken', {
    from: admin,
    args: [name, symbol],
    log: true,
    autoMine: true, // speed up deployment on local network (ganache, hardhat), no effect on live networks
  });

  try {
    if(hre.network.name != "localhost" && hre.network.name != "hardhat"){
      await hre.run("verify:verify", {
          address: contract.address,
          constructorArguments: [
            name, symbol
          ],
        });
    };
  } catch(e) {
    console.log(e);
  }
};
export default func;
func.tags = ['EgoToken'];
