import {expect} from './chai-setup';
import {ethers, deployments, getUnnamedAccounts, getNamedAccounts} from 'hardhat';
import {IERC20, DistributorV2} from '../typechain';
import {setupUser, setupUsers} from './utils';

const setup = deployments.createFixture(async () => {
  await deployments.fixture(['EgoToken', 'DistributorV2']);
  const {admin, validator, distributionCreator} = await getNamedAccounts();
  
  const contracts = {
    EgoToken: <IERC20>await ethers.getContract('EgoToken'),
    DistributorV2: <DistributorV2>await ethers.getContract('DistributorV2')
  };
  const users = await setupUsers(await getUnnamedAccounts(), contracts);
  return {
    ...contracts,
    users,
    admin: await setupUser(admin, contracts),
    validator: await setupUser(validator, contracts),
    distributionCreator: await setupUser(distributionCreator, contracts),
  };
});

describe('Distributor', function () {
  it('transfer fails', async function () {
    const {users} = await setup();
    await expect(users[0].EgoToken.transfer(users[1].address, 1)).to.be.revertedWith('ERC20: transfer amount exceeds balance');
  });

  it('transfer succeed', async function () {
    const {users, admin, EgoToken} = await setup();
    await admin.EgoToken.transfer(users[1].address, 1);
    
    await expect(admin.EgoToken.transfer(users[1].address, 1))
      .to.emit(EgoToken, 'Transfer')
      .withArgs(admin.address, users[1].address, 1);
  });

  it('Create distribution', async function () {
    const {admin, distributionCreator, DistributorV2} = await setup();
    let totalRewardAmount = ethers.utils.parseEther("10");
    await admin.EgoToken.transfer(distributionCreator.address, totalRewardAmount);

    await distributionCreator.EgoToken.approve(DistributorV2.address, totalRewardAmount);

    await expect(distributionCreator.DistributorV2.createDistribution(totalRewardAmount))
      .to.emit(DistributorV2, 'DistributionStarted')
      .withArgs(distributionCreator.address, totalRewardAmount);

    expect(await admin.EgoToken.balanceOf(DistributorV2.address)).to.be.eq(totalRewardAmount)
  });

  it('Claim by users', async function () {
    const {users, admin, validator, distributionCreator, DistributorV2, EgoToken} = await setup();
    let totalRewardAmount = ethers.utils.parseEther("10");
    let types = ["address", "uint256", 'uint256', 'uint256'];
    let  values, hash, signedTx;
    let chainId = 31337;
    let amountRewardForUser = totalRewardAmount.div(users.length);

    await admin.EgoToken.transfer(distributionCreator.address, totalRewardAmount);
    await distributionCreator.EgoToken.approve(DistributorV2.address, totalRewardAmount);
    await distributionCreator.DistributorV2.createDistribution(totalRewardAmount)

    for (let i = 0; i < users.length; i++) {
        values = [users[i].address, amountRewardForUser, chainId, i];
        hash = ethers.utils.solidityKeccak256(types, values);

        signedTx = await validator.DistributorV2.signer.signMessage(ethers.utils.arrayify(hash));

        let { v, r, s } = ethers.utils.splitSignature(signedTx);    

        await expect(users[i].DistributorV2.claim(amountRewardForUser, i, v, r,s))
        .to.emit(DistributorV2, 'Claim')
        .withArgs(users[i].address, amountRewardForUser)
        .and.to.emit(EgoToken, 'Transfer')
        .withArgs(DistributorV2.address, users[i].address, amountRewardForUser);
        expect(await admin.EgoToken.balanceOf(users[i].address)).to.be.eq(amountRewardForUser)
    }
  });
});