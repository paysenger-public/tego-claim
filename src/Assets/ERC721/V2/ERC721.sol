// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Royalty.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./RoyaltyContract.sol";

contract ERC721Collection is ERC721, ERC721Enumerable, ERC721URIStorage, Pausable, AccessControl, ERC721Royalty {
    using Counters for Counters.Counter;

    address public royaltyContract;
    bytes32 salt;

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");
    Counters.Counter private _tokenIdCounter;

    constructor() ERC721("MyToken", "MTK") {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(PAUSER_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    mapping(uint256 => address) royaltyReceiver;

    function deployRoyaltyContract(uint256 _percentOfReq, uint256 _percentOfRes, address _req, address _res) public returns(address){
        RoyaltyContract royaltyContractAddress = new RoyaltyContract(
            _percentOfReq,
            _percentOfRes,
            _req,
            _res
        );
        return address(royaltyContractAddress);
    }
    
    function safeMint(address to, string memory uri, uint96 _percentOfReq, uint96 _percentOfRes, address _req, address _res) public {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();

        address deployedRoyaltyContract = deployRoyaltyContract(_percentOfReq, _percentOfRes, _req, _res);

        _setTokenRoyalty(tokenId, deployedRoyaltyContract, _percentOfReq );
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        whenNotPaused
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    // The following functions are overrides required by Solidity.
    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage, ERC721Royalty) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable, AccessControl, ERC721Royalty)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}