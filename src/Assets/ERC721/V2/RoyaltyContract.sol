// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract RoyaltyContract {
    uint256 percentOfReq;
    uint256 percentOfRes;
    address req;
    address res;

    constructor(uint256 _percentOfReq, uint256 _percentOfRes, address _req, address _res) public {
        req = _req;
        res = _res;
        percentOfReq = _percentOfReq;
        percentOfRes = _percentOfRes;
    }   

    function withdrawNative(address payable receiver, uint256 amount) public {
        sendNative(receiver, amount);
    }

    function sendNative(address payable account, uint256 amount) private {
        (bool sent, ) = account.call{gas: 10000, value: amount}("");
        require(sent, "Failed to send Ether");
    }

    function withdrawERC20(
        address _token,
        address receiver,
        uint256 amount
    ) public {
        IERC20(_token).transfer(receiver, amount);
    }



    receive() external payable {

    }
}