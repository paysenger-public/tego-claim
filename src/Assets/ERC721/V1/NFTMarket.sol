// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "./NFTGallery.sol";

contract NFTMarket is NFTGallery {
    using Counters for Counters.Counter;

    struct Offer {
        uint256 tokenId;
        address seller;
        uint256 minValue;
    }

    mapping (uint256 => Offer) public tokensOfferedForSale;
    mapping (address => uint256) public pendingWithdrawals;

    event ForSale(uint256 indexed tokenId, uint256 minValue);
    event bought(uint256 indexed tokenId, uint256 value, address from, address to);

    function tokenPrice(uint256 tokenId) public view returns(uint256 price){
        price = _tokenPrice(tokenId);
        require(price > 0, "token is not for sale");
    }

    function _tokenPrice(uint256 tokenId) internal override view returns(uint256){
        return tokensOfferedForSale[tokenId].minValue;
    }

    function mintAndOffer(address to, string memory uri, uint256 minSalePriceInWei) public {
        safeMint(to, uri);
        uint256 tokenId = _tokenIdCounter.current() - 1;
        if(minSalePriceInWei > 0) {
            tokensOfferedForSale[tokenId] = Offer(tokenId, to, minSalePriceInWei);
            emit ForSale(tokenId, minSalePriceInWei);
        }
    }

    function offerToken(uint256 tokenId, uint256 minSalePriceInWei) public {
        require(_isApprovedOrOwner(_msgSender(), tokenId), "NFTMarket : you do not have access to this token");
        tokensOfferedForSale[tokenId] = Offer(tokenId, ownerOf(tokenId), minSalePriceInWei);
        emit ForSale(tokenId, minSalePriceInWei);
    }

    function buyToken(uint256 tokenId) public payable {
        Offer storage offer = tokensOfferedForSale[tokenId];
        uint256 availableAmount = msg.value;
        require(availableAmount >= tokenPrice(tokenId), "Insufficient amount to pay");

        //pay royalty
        (address royaltyReceiver, uint256 royaltyAmount) = royaltyInfo(tokenId, availableAmount);
        if (royaltyAmount > 0) {
            availableAmount -= royaltyAmount;
            pendingWithdrawals[royaltyReceiver] += availableAmount;
        }

        //pay market commission
        if (marketCommission.royaltyFraction > 0) {
            uint256 commission = (msg.value * marketCommission.royaltyFraction) / _feeDenominator();
            availableAmount -= commission;
            pendingWithdrawals[marketCommission.receiver] += commission;
        }

        //pay main value
        address seller = offer.seller;
        _safeTransfer(seller, _msgSender(), tokenId, "");
        pendingWithdrawals[seller] += availableAmount;
        offer.minValue = 0;
        emit bought(tokenId, msg.value, seller, _msgSender());
    }

    function withdraw() public {
        uint256 amount = pendingWithdrawals[_msgSender()];
        pendingWithdrawals[_msgSender()] = 0;
        payable(msg.sender).transfer(amount);
    }

    RoyaltyInfo public marketCommission;

    bytes32 public constant MARKET_ACCESS = keccak256("MARKET_ACCESS");

    function setMarketCommission(address receiver, uint96 feeNumerator) public onlyRole(MARKET_ACCESS) {
        require(feeNumerator <= _feeDenominator(), "ERC2981: royalty fee will exceed salePrice");
        require(receiver != address(0), "ERC2981: invalid receiver");

        marketCommission = RoyaltyInfo(receiver, feeNumerator);
    }
}