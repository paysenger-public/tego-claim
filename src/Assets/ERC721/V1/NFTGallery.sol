// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Royalty.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "./ERC721CreationRecord.sol";

contract NFTGallery is ERC721, ERC721Enumerable, ERC721URIStorage, ERC721Burnable, ERC721Royalty, Pausable, AccessControl, ERC721CreationRecord {
    using Counters for Counters.Counter;

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    Counters.Counter internal _tokenIdCounter;

    event Mint(uint256 indexed tokenId, address indexed creator, string uri);

    constructor() ERC721("NFTGallery", "G") {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(PAUSER_ROLE, msg.sender);
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    function allTokensOfOwner(address _owner) public view returns(
        uint[] memory ids,
        address[] memory creators,
        string[] memory uris,
        uint256[] memory prices
    ){
        uint256 arrayLength = balanceOf(_owner);
        ids = new uint[](arrayLength);
        creators = new address[](arrayLength);
        uris = new string[](arrayLength);
        prices = new uint[](arrayLength);
        for(uint index; index < arrayLength; index++){
            uint256 tokenId = tokenOfOwnerByIndex(_owner, index);
            ids[index] = tokenId;
            creators[index] = creatorOf(tokenId);
            uris[index] = tokenURI(tokenId);
            prices[index] = _tokenPrice(tokenId);
        }
    }

    function tokenIds(int _offset, int _length) external view returns(uint[] memory ids) {
        uint absoluteLength = uint(_length >= 0 ? _length : -_length);
        ids = new uint[](absoluteLength);
        int start = _offset >= 0 ? _offset : int(_tokenIdCounter.current()) + _offset;
        int8 step = _length >= 0 ? int8(1) : -1;
        uint j;
        for(int i = start; i != start + _length; i += step){
            uint id = uint(i);
            if(_exists(id)){
                ids[j] = id;
                j++;
            }
        }
    }

    function tokenInfoBatch(int _offset, int _length) external view returns(
        uint[] memory ids,
        address[] memory creators,
        address[] memory owners,
        string[] memory uris,
        uint[] memory prices
    ) {
        uint absoluteLength = uint(_length >= 0 ? _length : -_length);
        ids = new uint[](absoluteLength);
        creators = new address[](absoluteLength);
        owners = new address[](absoluteLength);
        uris = new string[](absoluteLength);
        prices = new uint[](absoluteLength);
        int start = _offset >= 0 ? _offset : int(_tokenIdCounter.current()) + _offset;
        int8 step = _length >= 0 ? int8(1) : -1;
        uint j;
        for(int i = start; i != start + _length; i += step) {
            uint id = uint(i);
            if(_exists(id)){
                ids[j] = id;
                (creators[j], owners[j], uris[j], prices[j]) = tokenInfo(id);
                j++;
            }
        }
    }

    function tokenInfo(uint256 tokenId) public view returns(
        address creator,
        address owner,
        string memory uri,
        uint256 price
    ) {
        creator = creatorOf(tokenId);
        owner = ownerOf(tokenId);
        uri = tokenURI(tokenId);
        price = _tokenPrice(tokenId);
    }

    function safeMintBatch(address[] memory to, string[] memory uri) public {
        require(to.length == uri.length, "NFTGallery: arrays should be in the same length.");
        for (uint256 i = 0; i < uri.length; i++) {
            safeMint(to[i], uri[i]);
        }
    }

    function safeMint(address to, string memory uri) public {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
        _recordCreationData(tokenId, block.timestamp, to);
        emit Mint(tokenId, to, uri);
    }

    function setTokenRoyalty(
        uint256 tokenId,
        address receiver,
        uint96 feeNumerator
    ) public virtual onlyCreator(tokenId) {
        require(feeNumerator <= 1000, "BanTheBanNFT: token royalty can be set up to 10 percent");
        _setTokenRoyalty(tokenId, receiver, feeNumerator);
    }

    function resetTokenRoyalty(uint256 tokenId) public virtual onlyCreator(tokenId) {
        _resetTokenRoyalty(tokenId);
    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        whenNotPaused
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _tokenPrice(uint256 tokenId) internal virtual view returns(uint256){}

    // The following functions are overrides required by Solidity.

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage, ERC721Royalty, ERC721CreationRecord) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable, ERC721Royalty, AccessControl)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}