//TODO: can we change royalty numerator
//TODO: can we burn token?

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Royalty.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

contract ERC721ReqCreator is ERC721, ERC721Enumerable, ERC721URIStorage, Pausable, AccessControl, ERC2981 {
    using Counters for Counters.Counter;

    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");
    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    Counters.Counter private _tokenIdCounter;

    constructor() ERC721("MyToken", "MTK") {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _grantRole(PAUSER_ROLE, msg.sender);
        _grantRole(MINTER_ROLE, msg.sender);
    }

    function pause() public onlyRole(PAUSER_ROLE) {
        _pause();
    }

    function unpause() public onlyRole(PAUSER_ROLE) {
        _unpause();
    }

    //is address to can be creator?
    function safeMintBatch(
        address[] calldata _to,
        string[] calldata _uri,
        uint96[] calldata _feeNumerators,
        address[] calldata _creators
    ) public {
        require(
            _to.length == _uri.length && _feeNumerators.length == _creators.length && _creators.length == _to.length,
            "NFTGallery: arrays should be in the same length."
        );

        for (uint256 i = 0; i < _uri.length; i++) {
            safeMint(_to[i], _uri[i], _feeNumerators[i], _creators[i]);
        }
    }

    function safeMint(
        address to,
        string calldata uri,
        uint96 _feeNumerator,
        address _creator
    ) public {
        require(_feeNumerator <= 1000, "BanTheBanNFT: token royalty can be set up to 10 percent");

        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();

        _setTokenRoyalty(tokenId, _creator, _feeNumerator);
        _safeMint(to, tokenId);
        _setTokenURI(tokenId, uri);
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 tokenId
    ) internal override(ERC721, ERC721Enumerable) whenNotPaused {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    // The following functions are overrides required by Solidity.
    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId) public view override(ERC721, ERC721URIStorage) returns (string memory) {
        return super.tokenURI(tokenId);
    }

    function allTokensOfOwner(address _owner) public view returns (uint256[] memory ids, string[] memory uris) {
        uint256 arrayLength = balanceOf(_owner);
        ids = new uint256[](arrayLength);
        uris = new string[](arrayLength);
        for (uint256 index; index < arrayLength; index++) {
            uint256 tokenId = tokenOfOwnerByIndex(_owner, index);
            ids[index] = tokenId;
            uris[index] = tokenURI(tokenId);
        }
    }

    function tokenIds(int256 _offset, int256 _length) external view returns (uint256[] memory ids) {
        uint256 absoluteLength = uint256(_length >= 0 ? _length : -_length);
        ids = new uint256[](absoluteLength);
        int256 start = _offset >= 0 ? _offset : int256(_tokenIdCounter.current()) + _offset;
        int8 step = _length >= 0 ? int8(1) : -1;
        uint256 j;
        for (int256 i = start; i != start + _length; i += step) {
            uint256 id = uint256(i);
            if (_exists(id)) {
                ids[j] = id;
                j++;
            }
        }
    }

    function tokenInfoBatch(int256 _offset, int256 _length)
        external
        view
        returns (
            uint256[] memory ids,
            address[] memory owners,
            string[] memory uris
        )
    {
        uint256 absoluteLength = uint256(_length >= 0 ? _length : -_length);
        ids = new uint256[](absoluteLength);
        owners = new address[](absoluteLength);
        uris = new string[](absoluteLength);
        int256 start = _offset >= 0 ? _offset : int256(_tokenIdCounter.current()) + _offset;
        int8 step = _length >= 0 ? int8(1) : -1;
        uint256 j;
        for (int256 i = start; i != start + _length; i += step) {
            uint256 id = uint256(i);
            if (_exists(id)) {
                ids[j] = id;
                (owners[j], uris[j]) = tokenInfo(id);
                j++;
            }
        }
    }

    function tokenInfo(uint256 tokenId) public view returns (address owner, string memory uri) {
        owner = ownerOf(tokenId);
        uri = tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable, AccessControl, ERC2981)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}
