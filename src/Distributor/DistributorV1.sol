// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";

contract Vesting is AccessControl {
    using ECDSA for bytes32;

    bytes32 public constant ADMIN_ROLE = keccak256("ADMIN_ROLE");

    address public token;
    
    //Messages that have already been used 
    //messages[messageHash] = true or false
    mapping(bytes32 => bool) messages;

    Vesting[] public vestings;

    enum VestingState {
        Created,
        Active,
        Complited
    }

    struct Vesting {
        uint256 startIn; // Vesting start time
        uint256 endIn; // Vesting  end time
        uint256 totalRewardAmount; // total amount of tokens destributed in vesting
        uint256 currentRewardAmount; // total amount of tokens destributed in vesting

        VestingState State; // Current state
    }

    event VestingCreated(address _creator, uint256 startIn, uint256 endIn, uint256 rewardAmount);

    event Claim(address _receiver, uint256 amount);

    //who can create vesting
    //ends in ?
    
    /**
     * @dev Contract constructor
     */
    constructor(address _token) {
        _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
        _setupRole(ADMIN_ROLE, msg.sender);
        token = _token;
    }

    function createVesting(
        uint256 _startIn,
        uint256 _endIn,
        uint256 _rewardAmount
    ) external {
        require(hasRole(ADMIN_ROLE, msg.sender), "Caller is not an admin.");
        require(
            _startIn < _endIn,
            "The start campaign time must be less then end time."
        );
        require(
            block.timestamp < _startIn,
            "The start campaign time must be in the future."
        );

        {
            uint256 initBalance = IERC20(token).balanceOf(
                address(this)
            );

            IERC20(token).transferFrom(
                msg.sender,
                address(this),
                _rewardAmount
            );

            uint256 finalBalance = IERC20(token).balanceOf(
                address(this)
            ) - initBalance;

            if (_rewardAmount != finalBalance) {
                _rewardAmount = finalBalance;
            }
        }

        vestings.push(
            Vesting({
                startIn: _startIn,
                endIn: _endIn,
                totalRewardAmount: _rewardAmount,
                currentRewardAmount: _rewardAmount,
                State: VestingState.Active
            })
        );
    
        emit VestingCreated(msg.sender, _startIn, _endIn, _rewardAmount);
    }

    function claim(
        uint256 _vestingId,
        uint256 _amount,
        address _receiver,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external {
        bytes32 message = keccak256(
            abi.encodePacked(msg.sender, _vestingId, _amount, _receiver, block.chainid)
        );
        require(messages[message] == false, "Transaction in process");
        require(
            hasRole(
                ADMIN_ROLE,
                message.toEthSignedMessageHash().recover(v, r, s)
            ),
            "Admin address is invalid"
        );
        Vesting storage vesting = vestings[_vestingId];
        require(vesting.currentRewardAmount >= _amount, "Not enough tokens in vesting");
        require(
            block.timestamp < vesting.endIn,
            "The start campaign time must be in the future."
        );
        vesting.currentRewardAmount -= _amount;

        IERC20(token).transferFrom(
                address(this),
                _receiver,
                _amount
            );

        emit Claim(_receiver, _amount);
    }
}
