import "@nomiclabs/hardhat-ethers";
import { task } from "hardhat/config";

task("transferERC20", "To transfer tokens to target")
  .addParam("to", "The account's address")
  .addParam("amount", "The account's address")
  .setAction(async (taskArgs, hre) => {
    const accounts = await hre.ethers.getSigners();
    console.log("Account:", accounts[1].address);

    const erc20 = await hre.ethers.getContract(
      "Test20"
      );
    console.log(
      "ERC20 address:",
      erc20.address
      );

    let result = await erc20
      .connect(accounts[1])
      .transfer(taskArgs.to, taskArgs.amount);
    let receipt = await result.wait()

    console.log("Transaction hash", receipt.transactionHash);
  });
