import "@nomiclabs/hardhat-ethers";
import { utils } from "ethers";
import { task } from "hardhat/config";
import { HardhatRuntimeEnvironment } from 'hardhat/types';


task("createDistribution", "To mint tokens to target").setAction(
  async (taskArgs, hre: HardhatRuntimeEnvironment) => {
    const accounts = await hre.ethers.getSigners();

    console.log("Account:", accounts[1].address);

    let amount = utils.parseUnits("100000000", 18);

    const erc20Token = await hre.ethers.getContract("EgoToken");
    let transferTx = await erc20Token.connect(accounts[0]).transfer(accounts[1].address, amount);
    await transferTx.wait()

    const distributor = await hre.ethers.getContract("DistributorV2");
    console.log("DistributorV2 address:", distributor.address);

    let approveTx = await erc20Token.connect(accounts[1]).approve(distributor.address, amount);
    await approveTx.wait()
    console.log("approve");

    const validatorRole = hre.ethers.utils.solidityKeccak256(["string"],
      ["VALIDATOR_ROLE"]);

    let tx = await distributor.connect(accounts[1]).createDistribution(amount);
    console.log(await tx.wait());
    // let tx = await distributor.connect(accounts[0]).grantRole(validatorRole,"0x703632A0b52244fAbca04aaE138fA8EcaF72dCBC");
    // console.log(await tx.wait());

  }
);