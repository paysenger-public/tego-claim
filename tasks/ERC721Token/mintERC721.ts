import "@nomiclabs/hardhat-ethers";
import { task } from "hardhat/config";

task("mintERC721", "To mint tokens to target")
  .setAction(async (taskArgs, hre) => {
    const accounts = await hre.ethers.getSigners();
    console.log("Account:", accounts[0].address);

    const erc721 = await hre.ethers.getContract("ERC721ReqCreator");
    console.log("ERC721 address:", erc721.address);
    let feeNumerator = 1000;
    let creator = "0x56Dc3F6E88a3DE1E8Abb76c44a7c614AB7961dE1";
    let result = await erc721
      .connect(accounts[0])
      .safeMint(
        accounts[0].address,
        "https://gateway.pinata.cloud/ipfs/QmcQN3CTpsL4LtKneY2i246J61kKez7JREDyhyUbLn7aGx",
        feeNumerator,
        creator
      );
    let receipt = await result.wait();

    console.log("Transaction hash", receipt.transactionHash);
    return receipt;
  });
