import { task } from "hardhat/config";

task("getInfoERC721", "To mint tokens to target")
  .addParam("tokenId", "The account's address")
  .setAction(async (taskArgs, hre) => {
    const accounts = await hre.ethers.getSigners();

    console.log("Account:", accounts[1].address);
    
    const erc721 = await hre.ethers.getContract("Test721");
    console.log("ERC721 address:", erc721.address);
    let name = await erc721.name();
    let symbol = await erc721.symbol();

    let tokenURI = await erc721.connect(accounts[1]).tokenURI(taskArgs.tokenId);

    let tokenURIs = await erc721
      .connect(accounts[1])
      .tokenURIs(taskArgs.tokenId);

    let ownerOf = await erc721.connect(accounts[1]).ownerOf(taskArgs.tokenId);
    let balanceOf = await erc721
      .connect(accounts[1])
      .balanceOf(accounts[0].address);
    let isApprovedForAll = await erc721.isApprovedForAll(
      accounts[2].address,
      accounts[0].address
    );
    let getApproved = await erc721.getApproved(taskArgs.tokenId);

    console.log("----------------Start_-----------------");
    console.log("Name", name);
    console.log("---------------------------------------");
    console.log("Symbol", symbol);
    console.log("---------------------------------------");
    console.log("baseTokenURI", tokenURI);
    console.log("---------------------------------------");
    console.log("TokenURI", tokenURIs);
    console.log("---------------------------------------");
    console.log("ownerOf", ownerOf);
    console.log("---------------------------------------");
    console.log("balanceOf", balanceOf);
    console.log("---------------------------------------");
    console.log("isApprovedForAll", isApprovedForAll);
    console.log("---------------------------------------");
    console.log("getApproved", getApproved);
    console.log("----------------End--------------------");
  });
