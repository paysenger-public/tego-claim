import './createDistribution.ts'

//tasks for ERC721 token
import './ERC721Token/getInfoERC721.ts'
import './ERC721Token/mintERC721.ts'
import './ERC721Token/approveERC721.ts'
import './ERC721Token/transferFromERC721.ts'

//tasks for ERC20 token
import './ERC20Token/getInfoERC20.ts'
import './ERC20Token/mintERC20.ts'
import './ERC20Token/transferERC20.ts'
import './ERC20Token/approveERC20.ts'
import './ERC20Token/transferFromERC20.ts'
